#!/usr/bin/make -f

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

DEB_HOST_GNU_TYPE   ?= $(shell dpkg-architecture -qDEB_HOST_GNU_TYPE)
DEB_BUILD_GNU_TYPE  ?= $(shell dpkg-architecture -qDEB_BUILD_GNU_TYPE)

ifeq ($(DEB_BUILD_GNU_TYPE), $(DEB_HOST_GNU_TYPE))
  conf_gnu_type += --build $(DEB_HOST_GNU_TYPE)
else
  conf_gnu_type += --build $(DEB_BUILD_GNU_TYPE) --host $(DEB_HOST_GNU_TYPE)
endif

CFLAGS = -Wall -g

ifneq (,$(findstring noopt,$(DEB_BUILD_OPTIONS)))
	CFLAGS += -O0
else
	CFLAGS += -O2
endif

include /usr/share/quilt/quilt.make

config.status: configure
	dh_testdir
	
	./configure \
	  $(conf_gnu_type) \
	  --prefix=/usr \
	  --mandir=\$${prefix}/share/man

build: patch config.status
	dh_testdir
	
	$(MAKE) CFLAGS="$(CFLAGS)"

clean: unpatch
	dh_testdir
	dh_testroot
	
	[ ! -f Makefile ] || $(MAKE) distclean
	
	[ ! -r /usr/share/misc/config.sub ] || \
	  cp -fu /usr/share/misc/config.sub config.sub
	[ ! -r /usr/share/misc/config.guess ] || \
	  cp -fu /usr/share/misc/config.guess config.guess
	
	dh_clean

install: build
	dh_testdir
	dh_testroot
	dh_clean -k
	dh_installdirs
	
	$(MAKE) install prefix=$(CURDIR)/debian/tmp/usr

binary-indep: build install
# Nothing to do.

binary-arch: build install
	dh_testdir
	dh_testroot
	
	dh_install -s --sourcedir=$(CURDIR)/debian/tmp
	dh_installdocs -s
	dh_installexamples -s
	dh_installman -s
	dh_installchangelogs -s ChangeLog
	dh_link -s
	dh_strip -s
	dh_compress -s
	dh_fixperms -s
	dh_makeshlibs -s
	dh_installdeb -s
	dh_shlibdeps -s
	dh_gencontrol -s
	dh_md5sums -s
	dh_builddeb -s

binary: binary-indep binary-arch

.PHONY: build clean binary-indep binary-arch binary install

